﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates_1
{
    class Program
    {
        //here is the delegate

        //this delegate can now point to a method that has similar signature
        public delegate string combine_strings(string a, string b);

        //this is another delegate that will be later multicasted
        public delegate void return_string();

        //here is a method that has a signature similar to the above delegate
        public static string add_two_strings(string a,string b)
        {
            string final_string = "";
            final_string = a + b;

            return final_string;
        }

        //one more method for this stuff
        public static void hello_there_1()
        {
            Console.WriteLine(" This is frmo hello there 1");
        }

        public static void hello_there_2()
        {
            Console.WriteLine(" This is from hello there 2");
        }

        public static void hello_there_3()
        {
            Console.WriteLine(" This is from hello there 3");
        }

        static void Main(string[] args)
        {
            //eventually we will look at building events and callbacks
            //before that, we need to learn about delegates

            //delgates are like pointers to a method

            //I will point the delegate to the method
            combine_strings temp_delegate = add_two_strings;

            //now I will call the method add_two_strings via the delegate object
            string temp_delegate_answer = temp_delegate("hello ", "delegate");

            Console.WriteLine("looks like the delegate who gave me {0}", temp_delegate_answer);

            Console.WriteLine("let me do the single and multi delgate casting ");

            return_string single_del = hello_there_1;  //I have casted the method to that delegate object

            single_del();  //this is a single casting thing

            return_string multi_del = hello_there_1;
            multi_del += hello_there_2;
            multi_del += hello_there_3;  //so, now the multi_del object has been casted into three methods.

            //let me see how many methods have been casted
            Console.WriteLine("the number of methods that are casted are {0}",multi_del.GetInvocationList().Count());

            //now when I call the delegate object, it will call all added methods
            multi_del();

            Console.WriteLine("let me do the lambda expressions thing ");

            //its possible to build anonymous methods using what are called lambda expressions
            //we have already used it when working on threads but lets get a little detail

            return_string lambda_del = () =>
            {
                Console.WriteLine("this is from the anonymous method written via lambda expressions"); ;
            };

            lambda_del();

            //here is another delegate that connects with an anomymous method with two parameters and return value
            //the following anonymous method is just like the add_two_strings() method
            //x and y are the two parameters
            //I dont have to mention the x and y type. the lambda thing will take care of it. 
            combine_strings lambda_del_return = (x, y) =>
            {
                return (x + y);
            };

            string lambda_combine_string = lambda_del_return("what is this", "anonymous thingy");

            Console.WriteLine(" came out out of that lambda combine - {0}", lambda_combine_string);

            //lets not let the console window dissapear
            Console.ReadLine();

        }
    }
}
